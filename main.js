function formatText(str) {
  var _str = str.split(' ');
  newStr = _str.map(function(s) {
    return s.charAt(0).toUpperCase() + s.slice(1);
  })
  return newStr.join('_')
}

function languageUrl(lang) {
  return 'http://' + lang + '.wikipedia.org/w/api.php?action=parse&format=json&origin=*&prop=text&section=0&page=';
}

function fetchWiki(e) {
  e.preventDefault();
  var topic, url
  var topicVal = document.getElementById('topic').value;
  var language = document.getElementById('language').value || 'en';
  if (!topicVal) {
    $('#content').html('<div><strong>Please, enter a title to search</div>');
  }
  topic = formatText(topicVal);
  url = languageUrl(language)  + topic + '&callback=?'
  $.ajax({
    type: 'GET',
    url: url,
    contentType: 'application/json; charset=utf-8',
    async: false,
    dataType: 'json',
    success: function (data, textStatus, jqXHR) {
      console.log('Page: ', data);
      if(data.error && data.error.code === 'missingtitle') {
        return $('#content').html('<div><strong>' + data.error.info + '<strong></div>');
      }
      var markup = data.parse.text['*'];
      var newDiv = $('<div></div>').html(markup);

      // spit out links
      newDiv.find('a').each(function () { $(this).replaceWith($(this).html()); });

      // remove any references
      newDiv.find('sup').remove();

      // spit out errors on citations
      newDiv.find('.mw-ext-cite-error').remove();
      $('#content').html($(newDiv).find('p'));
      return;
    },
    error: function (errorMessage) {
      return $('#content').html('<div><strong>Sorry, couldn\'t find content</div>');
    }
  });
}

// fetch(url, {
//     method: 'get',
//     headers: {
//       Accept: 'text/html',
//       Content_Type: 'application/json',
//       profile: 'https://www.mediawiki.org/wiki/Specs/HTML/1.4.0'
//     }
//   })
//   .then(function (response) {
//     console.log('Page', response.json());
//     var markup = data.parse.text["*"];
//     var newDiv = $('<div></div>').html(markup);

//     // remove links as they will not work
//     newDiv.find('a').each(function() { $(this).replaceWith($(this).html()); });

//     // remove any references
//     newDiv.find('sup').remove();

//     // remove cite error
//     newDiv.find('.mw-ext-cite-error').remove();
//     $('#content').html($(newDiv).find('p'));
//     // document.getElementById('content').innerHTML = response;
//   })
//   .catch(function (err) {
//     console.error(err)
//   });